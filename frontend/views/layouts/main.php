<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <nav style="background-color: rgb(64, 15, 53);">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo center">Logo</a>
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a href="#">Home</a></li>
            <li><a href="#">Experiencia</a></li>
            <li><a href="#">Trabajos</a></li>
          </ul>
          <ul class="side-nav" id="mobile-demo">
            <li><a href="#">Home</a></li>
            <li><a href="#">Experiencia</a></li>
            <li><a href="#">Trabajos</a></li>
          </ul>
        </div>
    </nav>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    

    <div class="container">
            <?= $content ?>
    </div>
</div>

<footer class="page-footer" style="background-color: rgb(64, 15, 53);position: absolute; bottom:0; width:100%; height: 70px;">
    <div class="footer-copyright">
        <div class="container">
            © 2015 Copyright Text Ady A. Prado S.
            <a class="grey-text text-lighten-4 right" >Telefono: (+58)4122335345</a>
        </div>
    </div>
</footer>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
